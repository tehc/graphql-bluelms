<?php

namespace App\Types;

use App\Entity\CurrentEnrolment;
use App\Entity\User;
use App\Repository\CurrentEnrolmentRepository;
use TheCodingMachine\GraphQLite\Annotations\ExtendType;
use TheCodingMachine\GraphQLite\Annotations\Field;

/**
 * @ExtendType(class=User::class)
 */
class UserType
{

    private $currentEnrolmentRepository;

    public function __construct(CurrentEnrolmentRepository $currentEnrolmentRepository)
    {
        $this->currentEnrolmentRepository = $currentEnrolmentRepository;
    }

    /**
     * @Field()
     * @param User $user 
     * @param bool|null $isCompleted 
     * @param bool|null $isWithdrawn 
     * @return CurrentEnrolment[] 
     */
    public function getCurrentEnrolments(User $user, bool $isCompleted = null, bool $isWithdrawn = null)
    {
        return $this->currentEnrolmentRepository->search($user, null, $isCompleted, $isWithdrawn)->getResult();
    }

}