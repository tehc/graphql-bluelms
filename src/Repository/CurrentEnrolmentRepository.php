<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\CurrentEnrolment;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use InvalidArgumentException;

/**
 * @method CurrentEnrolment|null find($id, $lockMode = null, $lockVersion = null)
 * @method CurrentEnrolment|null findOneBy(array $criteria, array $orderBy = null)
 * @method CurrentEnrolment[]    findAll()
 * @method CurrentEnrolment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrentEnrolmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CurrentEnrolment::class);
    }

    /**
     * @param User $user
     * @param Course $course
     * @param bool|null $isCompleted 
     * @param bool|null $isWithdrawn 
     * @return Query 
     * @throws InvalidArgumentException 
     */
    public function search(User $user = null, Course $course = null, bool $isCompleted = null, bool $isWithdrawn = null): Query
    {
        $qb = $this->createQueryBuilder('e');
        if (!is_null($isCompleted))
            $qb->andWhere('e.completion_status = :compstatus')->setParameter('compstatus', ($isCompleted ? 1 : 0));
        if (!is_null($isWithdrawn))
            $qb->andWhere('e.enrolment = :enrolstatus')->setParameter('enrolstatus', ($isWithdrawn ? '0' : '1'));
        if (!is_null($user))
            $qb->andWhere('e.user_id = :user')->setParameter('user', $user->getId());
        if (!is_null($course))
            $qb->andWhere('e.package_id = :course')->setParameter('course', $course->getId());
        return $qb->orderBy('e.id')->getQuery();
    }

}
