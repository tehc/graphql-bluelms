<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        //

        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @return Query|User[] Returns an array of User objects
     */
    public function search(?int $id, ?string $search): Query
    {
        $qb = $this->createQueryBuilder('u');
        if ($id)
            $qb->andWhere('u.l_id = :id')->setParameter('id', $id);
        elseif ($search)
            $qb->andWhere('u.l_uname LIKE :search')->setParameter('search', '%'.$search.'%');
        return $qb->orderBy('u.l_id', 'ASC')->getQuery();
    }
}
