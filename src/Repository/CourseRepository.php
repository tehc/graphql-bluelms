<?php

namespace App\Repository;

use App\Entity\Course;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    /**
     * @return Query|Course[] Returns an array of Course objects
     */
    public function search(?int $id, ?string $search): Query
    {
        $qb = $this->createQueryBuilder('c');
        if ($id)
            $qb->andWhere('c.id = :id')->setParameter('id', $id);
        elseif ($search)
            $qb->andWhere('c.name LIKE :search')->setParameter('search', '%'.$search.'%');
        return $qb->orderBy('c.id', 'ASC')->getQuery();
    }
}
