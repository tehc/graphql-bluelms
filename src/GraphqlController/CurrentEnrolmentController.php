<?php

namespace App\GraphqlController;

use App\Entity\Course;
use App\Entity\CurrentEnrolment;
use App\Entity\User;
use App\Repository\CourseRepository;
use App\Repository\CurrentEnrolmentRepository;
use App\Repository\UserRepository;
use InvalidArgumentException;
use Porpaginas\Doctrine\ORM\ORMQueryResult;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Right;
use TheCodingMachine\GraphQLite\Annotations\HideIfUnauthorized;


class CurrentEnrolmentController
{
    private $currentEnrolmentRepository;
    private $userRepository;
    private $courseRepository;

    public function __construct(
        CurrentEnrolmentRepository $currentEnrolmentRepository,
        UserRepository $userRepository,
        CourseRepository $courseRepository
    )
    {
        $this->currentEnrolmentRepository = $currentEnrolmentRepository;
        $this->userRepository = $userRepository;
        $this->courseRepository = $courseRepository;
    }

    /**
     * @Query()
     * @Right("ROLE_USER")
     * @param null|string $search 
     * @return CurrentEnrolment[]
     * @throws InvalidArgumentException 
     */
    public function currentEnrolments(?int $user = null, ?int $course = null, ?bool $isCompleted = null, ?bool $isWithdrawn = null): ORMQueryResult
    {
        if ($user)
            $user = $this->userRepository->find($user);
        if ($course)
            $course = $this->courseRepository->find($course);
        return new ORMQueryResult($this->currentEnrolmentRepository->search($user, $course, $isCompleted, $isWithdrawn));
    }
}