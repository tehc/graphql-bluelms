<?php

namespace App\GraphqlController;

use App\Entity\User;
use App\Repository\UserRepository;
use InvalidArgumentException;
use Porpaginas\Doctrine\ORM\ORMQueryResult;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Right;
use TheCodingMachine\GraphQLite\Annotations\HideIfUnauthorized;

class UserController
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Query()
     * @Right("ROLE_USER")
     * @param null|string $search 
     * @return User[]
     * @throws InvalidArgumentException 
     */
    public function users(?string $search): ORMQueryResult
    {
        return new ORMQueryResult($this->userRepository->search(null, $search));
    }

    /**
     * @Query()
     * @Right("ROLE_USER")
     * @param int $id 
     * @return User[]
     * @throws InvalidArgumentException 
     */
    public function user(int $id): ORMQueryResult
    {
        return new ORMQueryResult($this->userRepository->search($id, null));
    }
}