<?php

namespace App\GraphqlController;

use App\Entity\Course;
use App\Repository\CourseRepository;
use InvalidArgumentException;
use Porpaginas\Doctrine\ORM\ORMQueryResult;
use TheCodingMachine\GraphQLite\Annotations\Query;

class CourseController
{
    private $courseRepository;

    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * @Query()
     * @param null|string $search 
     * @return Course[]
     * @throws InvalidArgumentException 
     */
    public function courses(?string $search): ORMQueryResult
    {
        return new ORMQueryResult($this->courseRepository->search(null, $search));
    }

    /**
     * @Query()
     * @param null|string $search 
     * @return Course[]
     * @throws InvalidArgumentException 
     */
    public function course(?int $id): ORMQueryResult
    {
        return new ORMQueryResult($this->courseRepository->search($id, null));
    }
}