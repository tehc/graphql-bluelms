<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Security;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * @ORM\Table(name="multilevel_course")
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, unique=false)
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $description;

    /**
     * @ORM\Column(type="string")
     */
    private string $c_objective;

    /**
     * @ORM\Column(type="string")
     */
    private string $c_content;

    /**
     * @ORM\Column(type="string")
     */
    private string $audience;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('0', '1')")
     */
    private string $archive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CurrentEnrolment", mappedBy="course")
     * @ORM\JoinColumn(name="id", referencedColumnName="package_id")
     */
    private $currentEnrolments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Module", mappedBy="course")
     * @ORM\JoinColumn(name="id", referencedColumnName="course_id")
     * @var mixed
     */
    private $modules;

    /**
     * @Field()
     * @return null|int 
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @return string 
     */
    public function getName(): string
    {
        return (string) $this->name;
    }

    /**
     * @Field()
     * @return string 
     */
    public function getDescription(): string
    {
        return (string) $this->description;
    }

    /**
     * @Field()
     * @return string 
     */
    public function getObjectives(): string
    {
        return (string) $this->c_objective;
    }

    /**
     * @Field()
     * @return string 
     */
    public function getContent(): string
    {
        return (string) $this->c_content;
    }

    /**
     * @Field()
     * @return string 
     */
    public function getAudience(): string
    {
        return (string) $this->audience;
    }

    /**
     * @Field()
     * @return bool 
     */
    public function getIsArchived(): bool
    {
        return ($this->archive === '1');
    }

    /**
     * @Field()
     * @return Module[]
     */
    public function getModules()
    {
        return $this->modules;
    }

}