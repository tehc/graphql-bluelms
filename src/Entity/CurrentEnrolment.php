<?php

namespace App\Entity;

use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Security;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * @ORM\Table(name="assign_course")
 * @ORM\Entity(repositoryClass="App\Repository\CurrentEnrolmentRepository")
 */
class CurrentEnrolment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $package_id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $user_id;

    /** 
     * @ORM\Column(type="string", columnDefinition="ENUM('0', '1')")
     */
    private string $enrolment;

    /**
     * @ORM\Column(type="date")
     */
    private ?DateTime $start_date;

    /**
     * @ORM\Column(type="date")
     */
    private ?DateTime $end_date;

    /**
     * @ORM\Column(type="integer")
     */
    private int $completion_status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="currentEnrolments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="l_id")
     */
    private User $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="currentEnrolments")
     * @ORM\JoinColumn(name="package_id", referencedColumnName="id")
     */
    private Course $course;

    /**
     * @Field()
     * @return null|int 
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @return bool 
     */
    public function getIsWithdrawn(): bool
    {
        return ($this->enrolment === '0');
    }

    /**
     * @Field()
     * @return User 
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @Field()
     * @return Course 
     */
    public function getCourse(): Course
    {
        return $this->course;
    }

    /**
     * @Field()
     * @return DateTimeImmutable 
     */
    public function getStartDate(): ?DateTimeImmutable
    {
        if ($this->start_date)
            return (new DateTimeImmutable())->createFromMutable($this->start_date);
        return null;
    }

    /**
     * @Field()
     * @return DateTimeImmutable 
     */
    public function getExpiryDate(): ?DateTimeImmutable
    {
        if ($this->end_date)
            return (new DateTimeImmutable())->createFromMutable($this->end_date);
        return null;
    }

    /**
     * @Field()
     * @return bool 
     */
    public function getIsCompleted(): bool
    {
        return ($this->completion_status === 1);
    }

}