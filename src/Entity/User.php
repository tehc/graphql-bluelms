<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use TheCodingMachine\GraphQLite\Annotations\FailWith;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Security;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * @ORM\Table(name="login_details")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $l_id;

    /**
     * @ORM\Column(type="string", length=255, unique=false)
     */
    private string $l_uname;

    /**
     * @ORM\Column(type="string")
     */
    private string $l_pass;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private string $l_fname;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private string $l_lname;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private string $l_mail;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private ?int $l_cntry;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private ?int $l_state;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private ?int $l_city;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private ?string $l_phone;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private ?string $work_number;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private ?string $home_number;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private ?string $job_title;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private string $user_type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CurrentEnrolment", mappedBy="user")
     * @ORM\JoinColumn(name="l_id", referencedColumnName="user_id")
     */
    private $currentEnrolments;


    public function __construct(string $userName, string $email, string $plainPassword, UserPasswordEncoderInterface $passwordEncoder)
    {
        //
    }

    /**
     * @Field()
     * @return null|int 
     */
    public function getId(): ?int
    {
        return $this->l_id;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string
     */
    public function getUsername(): string
    {
        return (string) $this->l_uname;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string) $this->l_pass;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getFirstname(): string
    {
        return (string) $this->l_fname;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getLastname(): string
    {
        return (string) $this->l_lname;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getFullname(): string
    {
        return (string) $this->l_fname . ' ' . $this->l_lname;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getEmail(): string
    {
        return (string) $this->l_mail;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getMobilePhone(): string
    {
        return (string) $this->l_phone;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getHomePhone(): string
    {
        return (string) $this->home_number;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getWorkPhone(): string
    {
        return (string) $this->work_number;
    }

    /**
     * @Field()
     * @Security("is_logged() && (is_granted('ROLE_ADMIN') || user.l_id == this.l_id)")
     * @FailWith(null)
     * @return string 
     */
    public function getJobTitle(): string
    {
        return (string) $this->job_title;
    }

    /**
     * @return array 
     */
    public function getRoles(): array
    {
        return array_merge(['ROLE_USER'], (in_array($this->user_type, ['1', '2']) ? ['ROLE_ADMIN'] : null));
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
