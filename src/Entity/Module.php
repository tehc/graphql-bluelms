<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Security;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @Type()
 * @ORM\Table(name="scorm_module")
 * @ORM\Entity(repositoryClass="App\Repository\ModuleRepository")
 */
class Module
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private int $course_id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private string $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="modules")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     */
    private Course $course;

    /**
     * @Field()
     * @return null|int 
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Field()
     * @return string 
     */
    public function getName(): string
    {
        return (string) $this->name;
    }

    /**
     * @Field()
     * @return string 
     */
    public function getDescription(): string
    {
        return (string) $this->description;
    }

    /**
     * @Field()
     * @return Course 
     */
    public function getCourse(): Course
    {
        return $this->course;
    }
}